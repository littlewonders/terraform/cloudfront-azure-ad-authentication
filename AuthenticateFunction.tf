resource "null_resource" "authenticate-dependencies" {
  provisioner "local-exec" {
    command = "npm install"
    working_dir = "${path.module}/src/Authenticate"
  }

  triggers = {
    dependencies_versions = filemd5("${path.module}/src/Authenticate/package.json")
  }
}

module "authenticate" {
  source = "git::https://gitlab.com/littlewonders/terraform/lambda-at-edge.git?ref=v1.0.0"

  functions = {
    authenticate = {
      source       = "${path.module}/src/Authenticate"
      runtime      = "nodejs18.x"
      handler      = "index.handler"
      event_type   = "viewer-request"
      include_body = true
      params = {
        jwks       = jsondecode(data.http.jwks-configuration.response_body)
        config     = jsondecode(data.http.openid-configuration.response_body)
        client_id  = azuread_service_principal.main.application_id
        verify_key = random_password.jwtkey.result
      }
    }
  }

  providers = {
    aws = aws.us-east-1
  }

  context = module.this.context

  depends_on = [
    null_resource.authenticate-dependencies
  ]
}

data "http" "openid-configuration" {
  url = "https://login.microsoftonline.com/${data.azuread_client_config.current.tenant_id}/v2.0/.well-known/openid-configuration"

  request_headers = {
    Accept = "application/json"
  }
}

data "http" "jwks-configuration" {
  url = jsondecode(data.http.openid-configuration.response_body)["jwks_uri"]

  request_headers = {
    Accept = "application/json"
  }
}

resource "azuread_application" "app" {
  display_name = "${var.application_name} [${var.context.environment}]"
  owners       = [data.azuread_client_config.current.object_id]

  public_client {
    redirect_uris = ["${var.cloudfront_endpoint}/_cloudfront_auth/callback"]
  }

  api {
    mapped_claims_enabled = true
  }

  web {
    homepage_url = var.cloudfront_endpoint
    //redirect_uris = ["${var.cloudfront_endpoint}/_cloudfront_auth/callback"]

    implicit_grant {
      access_token_issuance_enabled = true
      id_token_issuance_enabled     = true
    }
  }
}

data "azuread_application_published_app_ids" "well_known" {}

resource "azuread_service_principal" "msgraph" {
  application_id = data.azuread_application_published_app_ids.well_known.result.MicrosoftGraph
  use_existing   = true
}

resource "azuread_service_principal_delegated_permission_grant" "adminconsent" {
  service_principal_object_id          = azuread_service_principal.main.object_id
  resource_service_principal_object_id = azuread_service_principal.msgraph.object_id
  claim_values                         = ["openid", "User.Read"]
}

data "azuread_client_config" "current" {}

data "azurerm_subscription" "primary" {
}

resource "azuread_service_principal" "main" {
  application_id               = azuread_application.app.application_id
  app_role_assignment_required = false
  owners                       = [data.azuread_client_config.current.object_id]

  login_url                     = var.cloudfront_endpoint
  preferred_single_sign_on_mode = "oidc"


  feature_tags {
    enterprise = true
  }
}

resource "azuread_service_principal_claims_mapping_policy_assignment" "app" {
  claims_mapping_policy_id = azuread_claims_mapping_policy.app.id
  service_principal_id     = azuread_service_principal.main.id
}

resource "azuread_claims_mapping_policy" "app" {
  definition = [
    jsonencode(
      {
        ClaimsMappingPolicy = {
          ClaimsSchema = [
            {
              ID           = "userprincipalname"
              JwtClaimType = "em"
              //SamlClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"
              Source = "user"
            }
          ]
          IncludeBasicClaimSet = "true"
          Version              = 1
        }
      }
    ),
  ]
  display_name = var.context.id
}

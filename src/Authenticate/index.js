const { Authenticator } = require('oidc-at-edge');

const params = require('./params.json');
const jwks = params.jwks;
const oidcConfiguration = params.config;

let authenticator_cache = null;

async function loadAuthenticator() {
    if (authenticator_cache) return authenticator_cache;

    authenticator_cache = new Authenticator({
        clientId: params.client_id,
        jwks,
        wellKnown: oidcConfiguration,
        logLevel: "debug",
        verifyKey: params.verify_key
    });

    return authenticator_cache;
}

exports.handler = async (request) => {
    const rq = request.Records[0].cf.request;

    if (rq.headers['x-bypass-backend-cloudfront'] && rq.headers['x-bypass-backend-cloudfront'][0].value === params.BYPASS_KEY) {
        console.log("Bypass used");
        return rq;
    }

    return await (await loadAuthenticator()).handle(request);
}
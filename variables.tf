variable "context" {
  type        = any
  description = "Context of a cloudposse/label resource"
  default     = {}
}

variable "name" {
  type        = string
  description = "Name to generate labels for resources"
}

variable "region" {
  type        = string
  description = "AWS region"
}

variable "profile" {
  type        = string
  description = "AWS profile"
}

variable "application_name" {
  type        = string
  description = "Name for Enterprise Application in Azure AD"
}

variable "cloudfront_endpoint" {
  type        = string
  description = "Domain that is configured with Cloudfront, eg. https://mysite.com"
}

variable "any_origin" {
  type        = string
  description = "Name of an origin on your Cloudfront distribution. Required for the cache behaviour to be generated"
}

variable "verify_template_location" {
  type        = string
  default     = ""
  description = "Which file to use for the Verify function"
}
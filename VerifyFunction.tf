resource "random_password" "jwtkey" {
  length  = 64
  special = false
}

resource "aws_cloudfront_function" "verify" {
  name    = "${module.this.id}-verify"
  runtime = "cloudfront-js-1.0"
  comment = "Validates the verifyToken cookie"
  publish = true
  code = templatefile(
    var.verify_template_location == "" ? "${path.module}/src/Verify/function.template.js" : var.verify_template_location,
    {
      key         = random_password.jwtkey.result,
      cookie_base = "idp.${azuread_service_principal.main.application_id}"
    }
  )
}
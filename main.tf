module "this" {
  source  = "cloudposse/label/null"
  version = "0.25.0"

  name    = var.name
  context = var.context
}

provider "aws" {
  region  = var.region
  profile = var.profile

  default_tags {
    tags = {
      service = "littlewonders-wordpress"
    }
  }
}
provider "aws" {
  region  = "us-east-1"
  profile = var.profile
  alias   = "us-east-1"

  default_tags {
    tags = {
      service = "littlewonders-wordpress"
    }
  }
}